#include <Arduino.h>
#include <cstdint>
#include <array>
#include "Config.hpp"
#include "GarageDoor.hpp"

#define SerialRS485 Serial2

static std::size_t bufferIndex = 0;
static std::array<std::uint8_t, Message::MAX_LENGTH> buffer = { 0 };

static std::size_t idleTimer = 0;

bool readByte(Message& msg) {
  static auto lastByte = micros();

  auto current = micros();
  if (SerialRS485.available()) {
    buffer[bufferIndex++] = SerialRS485.read();
    lastByte = current;
    return false;
  } else if (current - lastByte > 2000 && bufferIndex > Message::MIN_LENGTH) {
    // TODO: assert that buffer[0] is 0?
    // TODO: Assert: bufferIndex > MIN_LENGTH + 1 (because of TODO 1)
    // TODO: Assert that bufferIndex == MIN_LENGTH + 1 + msg.length
    //       (this can only be asserted after reading length, which is fine when the first two assertions succeeded)
    msg.address = buffer[1];
    msg.counter = (buffer[2] & 0xf0) >> 4;
    msg.length = buffer[2] & 0x0f;
    std::copy_n(buffer.begin() + 3, msg.length, msg.data.begin());
    msg.checksum = buffer[3 + msg.length];
    bufferIndex = 0;
    return buffer[0] == 0;
  } else {
    idleTimer++;
    return false;
  }
}

void SendMessage(const Message& msg) {
  delay(2);
  digitalWrite(Config::RS485::SendEnable, HIGH);
  const auto bytes = msg.bytes();
  SerialRS485.write(0x00);
  SerialRS485.write(bytes.begin(), bytes.size());
  SerialRS485.flush();
  digitalWrite(Config::RS485::SendEnable, LOW);
}

GarageDoor garageDoor(SendMessage);

void setup() {
  // put your setup code here, to run once:
  SerialRS485.setTxBufferSize(Message::MAX_DATA_LENGTH);
  SerialRS485.begin(19200, SERIAL_8N1, Config::RS485::RXD, Config::RS485::TXD);
  Serial.begin(115200);
  pinMode(Config::RS485::SendEnable, OUTPUT);
  pinMode(23, INPUT_PULLUP);

  while (!Serial && !SerialRS485)
  {
    // wait for Serial to be ready
  }

  Serial.println("Ready");
}

void PrintMessage(const Message& msg) {
  Serial.print("Address: ");
  Serial.println(msg.address, HEX);
  Serial.print("Counter: ");
  Serial.println(msg.counter);
  Serial.print("Length: ");
  Serial.println(msg.length);
  Serial.print("Data: ");
  for (std::size_t i = 0; i < msg.length; i++) {
    Serial.print(msg.data[i], HEX);
    Serial.print(" ");
  }
  Serial.println();
  Serial.print("Checksum: ");
  Serial.println(msg.checksum, HEX);
}

void loop() {
  // read byte from serial
  Message msg;
  if (readByte(msg) && msg.isCrcValid()) {
    garageDoor.newMessage(msg, digitalRead(23) == LOW);
  }

  static auto nextStatusUpdate = 0;
  if (nextStatusUpdate < millis()) {
    nextStatusUpdate = millis() + 2000;
    const auto status = garageDoor.getStatus();
    Serial.print("End Position Open: "); Serial.println(status.endPositionOpen);
    Serial.print("End Position Closed: "); Serial.println(status.endPositionClosed);
    Serial.print("Option Relay On: "); Serial.println(status.optionRelayOn);
    Serial.print("Light Relay On: "); Serial.println(status.lightRelayOn);
    Serial.print("Error Pending: "); Serial.println(status.errorPending);
    Serial.print("Moving Towards Closed Position: "); Serial.println(status.movingTowardsClosedPosition);
    Serial.print("Moving Towards Open Position: "); Serial.println(status.movingTowardsOpenPosition);
    Serial.print("Burglar Alarm: "); Serial.println(status.burglarAlarm);
    Serial.println();
  }
}