/**
 * @file GarageDoor.cpp
 * @author Julian Neundorf (https://gitlab.com/GURKE)
 * @brief
 * @version 0.1
 * @date 2024-06-17
 */
#include "GarageDoor.hpp"
#include "Message.hpp"
#include <cstdint>

namespace Address {
  constexpr std::uint8_t Broadcast = 0x00;
  constexpr std::uint8_t Host = 0x80;
  constexpr std::uint8_t Own = 0x29;
} // namespace Address

GarageDoor::GarageDoor(void (*SendMessage)(const Message&)) : SendMessage_(SendMessage) {

}

void GarageDoor::newMessage(Message& msg, bool openClose) {
  if (msg.address == Address::Broadcast) {
    if (msg.length != 1)
      return;

    status_.endPositionOpen = msg.data[0] & 0x01;
    status_.endPositionClosed = msg.data[0] & 0x02;
    status_.optionRelayOn = msg.data[0] & 0x04;
    status_.lightRelayOn = msg.data[0] & 0x08;
    status_.errorPending = msg.data[0] & 0x10;
    status_.movingTowardsClosedPosition = msg.data[0] & 0x20;
    status_.movingTowardsOpenPosition = msg.data[0] & 0x40;
    status_.burglarAlarm = msg.data[0] & 0x80;
  } else if (msg.address == Address::Own) {
    if (msg.data[0] == 0x01) { // Scan request
      // TODO: Instead of a constant host address maybe read from scan request
      const Message response(msg.data[1], 0x1, { 0x14, Address::Own });
      SendMessage_(response);
      hostAddress_ = msg.data[1];
    } else if (msg.data[0] == 0x20) { // Status request
      // TODO: Send command only once
      if (openClose && status_.endPositionClosed) // Open garage
        SendMessage_(Message(hostAddress_, 0x1, { 0x29, 0x01, 0x10 }));
      else if (openClose && status_.endPositionOpen) // Close garage
        SendMessage_(Message(hostAddress_, 0x1, { 0x29, 0x02, 0x10 }));
      else // Do nothing
        SendMessage_(Message(hostAddress_, 0x1, { 0x29, 0x00, 0x10 }));
    }
  }
}