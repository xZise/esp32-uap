#include <cstdint>

namespace Config::RS485 {
    constexpr std::int8_t RXD = 16;
    constexpr std::int8_t TXD = 17;
    constexpr std::int8_t SendEnable = 21;
}