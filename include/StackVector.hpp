// https://gitlab.com/GURKE/stackvector
/**
 * @file StackVector.hpp
 * @author Julian Neundorf
 * @brief A simple c++ vector like class with elements stored on stack
 * @version 0.1
 * @date 2023-11-12
 */

#pragma once

#include <array>
#include <cstddef>

template <typename T, std::size_t N>
class StackVector {
public:
  StackVector() {

  }

  T operator[](std::size_t i) {
    if (i >= numberOfElements)
      return values[numberOfElements - 1];
    else
      return values[i];
  }

  size_t size() const {
    return numberOfElements;
  }

  void push_back(const T& elem) {
    if (numberOfElements >= N)
      return;
    values[numberOfElements++] = elem;
  }

  void resize(std::size_t count) {
    if (count >= N)
      return;
    numberOfElements = count;
  }

  const T* begin() const {
    return values.cbegin();
  }

  const T* end() const {
    return (values.cbegin() + numberOfElements);
  }
private:
  std::array<T, N> values;
  std::size_t numberOfElements = 0;
};
