/**
 * @file Message.hpp
 * @author Julian Neundorf (https://gitlab.com/GURKE)
 * @brief
 * @version 0.1
 * @date 2024-06-17
 */
#pragma once

#include <algorithm>
#include <cstdint>
#include <initializer_list>
#include "StackVector.hpp"

class Message {
public:
  static constexpr std::size_t MAX_DATA_LENGTH = 15;
  static constexpr std::size_t MIN_LENGTH = 1 + 1 + 1;
  static constexpr std::size_t MAX_LENGTH = MIN_LENGTH + MAX_DATA_LENGTH;

  Message() {}

  Message(const std::uint8_t address, const std::uint8_t counter, const std::initializer_list<uint8_t> data)
    : address(address), counter(counter), length(data.size()) {
    std::copy_n(data.begin(), data.size(), this->data.begin());
    checksum = calculateChecksum(bytes().begin(), length + MIN_LENGTH - 1);
  }

  std::uint8_t address;
  std::uint8_t counter;
  std::uint8_t length;
  std::array<std::uint8_t, Message::MAX_DATA_LENGTH> data;
  std::uint8_t checksum;

  StackVector<std::uint8_t, Message::MAX_LENGTH> bytes() const {
    StackVector<std::uint8_t, Message::MAX_LENGTH> result;
    result.push_back(address);
    result.push_back((counter << 4) | length);
    for (size_t index = 0; index < length; index++) {
      result.push_back(data[index]);
    }
    result.push_back(checksum);
    return result;
  }

  bool isCrcValid() {
    return calculateChecksum(bytes().begin(), length + Message::MIN_LENGTH) == 0;
  }

private:
  template<typename T>
  uint8_t calculateChecksum(const T& bytes, std::size_t length) {
    const uint8_t polynomial = 0x07;
    uint8_t crc = 0xf3;
    for (std::size_t i = 0; i < length; i++) {
      crc ^= bytes[i];

      for (uint8_t i = 0; i < 8; i++) {
        if ((crc & 0x80) != 0) {
          crc = (uint8_t)((crc << 1) ^ polynomial);
        } else {
          crc <<= 1;
        }
      }
    }
    return crc;
  }
};