/**
 * @file GarageDoor.hpp
 * @author Julian Neundorf (https://gitlab.com/GURKE)
 * @brief
 * @version 0.1
 * @date 2024-06-17
 */
#pragma once
#include "Message.hpp"

class GarageDoor
{
public:
  GarageDoor(void (*SendMessage)(const Message&));
  void newMessage(Message& msg, bool openClose);

  struct Status {
    bool endPositionOpen = false;
    bool endPositionClosed = false;
    bool optionRelayOn = false;
    bool lightRelayOn = false;
    bool errorPending = false;
    bool movingTowardsClosedPosition = false;
    bool movingTowardsOpenPosition = false;
    bool burglarAlarm = false;
  };
  const Status& getStatus() { return status_; }

private:
  Status status_;
  std::uint8_t hostAddress_ = 0;

  void (*SendMessage_)(const Message&);
};